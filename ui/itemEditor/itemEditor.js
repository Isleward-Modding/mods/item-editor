define([
	'js/system/events',
	'js/system/client',
	'js/system/globals',
	'html!server/mods/item-editor/ui/itemEditor/template',
	'css!server/mods/item-editor/ui/itemEditor/styles',
	'js/input',
	'js/config',
	'ui/shared/renderItem'
], function (
	events,
	client,
	globals,
	template,
	styles,
	input,
	config,
	renderItem
) {
	return {
		tpl: template,

		centered: true,

		item: {
			name: 'Test Item',
			quality: 0,
			sprite: [0, 0],
			type: 'Item',
			isNew: true
		},

		modal: true,
		hasClose: true,

		hoverItem: null,

		vendorOptions: false,
		vendorDetails: {},

		postRender: function () {
			this.onEvent('onToggleQualityIndicators', this.onToggleQualityIndicators.bind(this));
			this.onToggleQualityIndicators(config.qualityIndicators);

			this.onEvent('onToggleUnusableIndicators', this.onToggleUnusableIndicators.bind(this));
			this.onToggleUnusableIndicators(config.unusableIndicators);

			this.onEvent('onKeyDown', this.onKeyDown.bind(this));

			// Update item when an input element is changed
			this.find('input').on('change', this.onChange.bind(this));

			this.find('.btnNew').on('click', function () {
				this.item.isNew = true;
				this.build();
			}.bind(this));
			this.find('.btnExport').on('click', function () {
				let exported = $.extend({}, this.item, this.vendorDetails);

				console.log(JSON.stringify(exported, null, 4));
			}.bind(this));
		},

		onChange: function () {
			this.item.name = this.find('.txtName').val();
			this.item.type = this.find('.txtType').val();
			this.item.slot = this.find('.txtSlot').val();
			this.item.quality = parseInt(this.find('.txtQuality').val());
			this.item.quantity = parseInt(this.find('.txtQuantity').val());
			this.item.level = parseInt(this.find('.txtLevel').val());
			this.item.description = this.find('.txtDescription').val();
			this.item.spritesheet = this.find('.txtSpriteSheet').val();
			this.item.sprite = [
				parseInt(this.find('.txtSpriteX').val()),
				parseInt(this.find('.txtSpriteY').val())
			];

			this.item.isNew = this.find('#chkNew').prop('checked');
			this.item.material = this.find('#chkMaterial').prop('checked');
			this.item.quest = this.find('#chkQuest').prop('checked');
			this.item.noSalvage = this.find('#chkNoSalvage').prop('checked');
			this.item.noDrop = this.find('#chkNoDrop').prop('checked');
			this.item.noDestroy = this.find('#chkNoDestroy').prop('checked');

			this.vendorDetails.worth = this.find('.txtVendorWorth').val();
			this.vendorDetails.generate = this.find('#chkVendorGenerate').prop('checked');
			this.vendorDetails.infinite = this.find('#chkVendorInfinite').prop('checked');

			this.vendorOptions = this.find('#chkShowVendor').prop('checked');
			this.find('.vendorOptions').css('display', this.vendorOptions ? 'block' : 'none');

			this.build();
		},

		build: function () {
			let container = this.el.find('.itemContainer')
				.empty();

			let item = this.item;

			let spritesheet = item.spritesheet || '../../../images/items.png';
			if (!item.spritesheet) {
				if (item.material)
					spritesheet = '../../../images/materials.png';
				else if (item.quest)
					spritesheet = '../../../images/questItems.png';
				else if (item.type === 'consumable')
					spritesheet = '../../../images/consumables.png';
				else if (item.type === 'skin')
					spritesheet = '../../../images/characters.png';
			}

			this
				.find('.spritesheet')
				.css('background', `url(${spritesheet})`)
				.css('background-size', 'contain')
				.css('background-repeat', 'no-repeat')

			let itemEl = renderItem(container, item);

			let moveHandler = this.onHover.bind(this, itemEl, item);

			itemEl
				.data('item', item)
				.on('mousemove', moveHandler)
				.on('mouseleave', this.hideTooltip.bind(this, itemEl, item))
		},

		onHover: function (el, item, e) {
			if (item)
				this.hoverItem = item;
			else
				item = this.hoverItem;

			if (!item)
				return;

			let ttPos = null;

			if (el) {
				// Only control the 'new' marker using checkbox
				// if (el.hasClass('new')) {
				// 	el.removeClass('new');
				// 	el.find('.quantity').html((item.quantity > 1) ? item.quantity : '');
				// 	delete item.isNew;
				// }

				ttPos = {
					x: ~~(e.clientX + 32),
					y: ~~(e.clientY)
				};
			}

			events.emit('onShowItemTooltip', item, ttPos, true);
		},

		onToggleQualityIndicators: function (state) {
			const className = `quality-${state.toLowerCase()}`;

			$('.ui-container')
				.removeClass('quality-off quality-bottom quality-border quality-background')
				.addClass(className);
		},

		onToggleUnusableIndicators: function (state) {
			const className = `unusable-${state.toLowerCase()}`;

			$('.ui-container')
				.removeClass('unusable-off unusable-border unusable-top unusable-background')
				.addClass(className);
		},

		hideTooltip: function () {
			if (this.dragEl) {
				this.hoverCell = null;
				return;
			}

			events.emit('onHideItemTooltip', this.hoverItem);
			this.hoverItem = null;
		},

		onAfterShow: function () {
			// Populate from html element's values
			this.onChange();
			
			this.hideTooltip();
		},

		beforeDestroy: function () {
			this.el.parent().css('background-color', 'transparent');
			this.el.parent().removeClass('blocking');
		},

		beforeHide: function () {
			this.hideTooltip();
		},

		onKeyDown: function (key) {
			// Hardcoded to open on N, maybe open with an NPC or command later?
			if (key === 'n')
				this.toggle();
		}
	};
});
