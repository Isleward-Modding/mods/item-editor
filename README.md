# Item Editor

Isleward mod that adds a fullscreen item editor to the Isleward client.

It's currently hardcoded to toggle when you press N.

## Extending

After adding the field to `template.html`, just add the "parsing" behavior to `onChange`.
All fields are watched and the item is rebuilt when any field is changed.

## Future Features

* noStash/noAugment checkboxes
* 2+ columns of checkboxes -- maybe can expand automatically to the most efficient layout?
* Disable shift comparison
* Give character the designed item
* Don't export useless variables (`infinite: false`, `noSalvage: false`, `quest: false`, etc)
* Import feature -- paste in existing item and edit it ---- maybe inventory item context to edit items directly in your inventory (?!?)
* Clear button
* Stats/rolls/ranges
* Slot dropdown
* +/- button styling for number fields
* Item effects
* attrRequire
* Factions (?)
* Show tooltip without shine effect on sprite (?)
* Pick sprite by clicking on spritesheet (?)
* Spritesheet list/dropdown (?) -- use clientconfig for additional images added by mods

![Screenshot](https://i.imgur.com/BmSx7vN.png)

