module.exports = {
	name: 'Item Editor',

	init: function () {
		this.events.on('onBeforeGetUiList', this.onBeforeGetUiList.bind(this));

		// this.events.on('onBeforeGetClientConfig', this.onBeforeGetClientConfig.bind(this));
		// this.events.on('onBeforeGetEffect', this.onBeforeGetEffect.bind(this));
		// this.events.on('onBeforeGetItemEffectList', this.onBeforeGetItemEffectList.bind(this));
		// this.events.on('onAfterGetZone', this.onAfterGetZone.bind(this));
	},

	onBeforeGetUiList: function (list) {
		list.push({
			path: 'server/mods/item-editor/ui/itemEditor'
		});
	}

	// onBeforeGetClientConfig: function ({ resourceList }) {
	// 	resourceList.push(`${this.folderName}/images/items.png`);
	// },
	//
	// onBeforeGetEffect: function (result) {
	// 	if (this.effects[result.type.toLowerCase()]) {
	// 		result.url = this.effects[result.type.toLowerCase()];
	// 	}
	// },
	//
	// onBeforeGetItemEffectList: function (list) {
	// 	this.itemEffects.forEach(function (e) {
	// 		list[e] = this.relativeFolderName + '/itemEffects/' + e + '.js';
	// 	}.bind(this));
	// },
	//
	// // To easily get the items
	// onAfterGetZone: function (zone, config) {
	// 	if (zone !== 'fjolarok')
	// 		return;
	//
	// 	let itemList = require('./items').getList();
	//
	// 	config.mobs.hermit.properties.cpnTrade = {
	// 		items: {
	// 			min: 0,
	// 			max: 0,
	// 			extra: itemList
	// 		},
	// 		markup: { buy: 0.25, sell: 0 },
	// 		faction: { id: 'fjolgard', tier: 5 } // why is this line necessary?
	// 	}
	// },
};
